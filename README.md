# Open3D para Reconstrução 3D em Python - Códigos Exemplos

Palestra disponível em: https://docs.google.com/presentation/d/19wK4ap1k5p6nD4T0sQBA121e0ekHvjMRWSBsoWIVApc/edit?usp=sharing

Este repositório contém os códigos dos exemplos da palestra "Open3D para Reconstrução 3D em Python", apresentada na Python Brasil 2020

* [ler_rgbd.py](ler_rgbd.py) como ler uma imagem colorida e de profundidade para criar
uma imagem RGBD, extrair sua nuvem de pontos e exibir;
* [registro.py](registro.py) Como registrar um par de imagens RGBD;
* [odometria.py](odometria.py) Odometria visual quadro-a-quadro. Estima a trajetória e exibe as nuvems de pontos alinhadas.
* [reconstrucao_cena.py](reconstrucao_cena.py) Demonstra como usar a odometria visual para reconstruir uma cena usando um volume TSDF.
* [reconstrucao_peca.py](reconstrucao_peca.py) Demonstra como usar a odometria visual para reconstruir uma peça usando um volume TSDF.

Contato: otavio.gomes@eldorado.org.br

# Obtendo

Utilize o `git clone`:

```shell
git clone https://otavio-gomes@bitbucket.org/eldoradocorp/reconstrucao3d-open3d-python.git
```

Para executar todos os exemplos os seguintes arquivos precisam ser descompactados:

```shell
unzip sequencia-rgbd.zip
unzip t-less.zip
```

# Testando os exemplos com Virtualenv

Requisitos:

* Python>=3.7

Testado no Ubuntu 18.04, com os seguintes pacotes:

```shell
sudo apt install python3 python3-venv build-essential python3-dev
```

Comandos para criar um virtualenv

```shell
python3 -m venv venv
source venv/bin/activate
pip3 install -U pip
pip3 install -r requirements.txt
```

# Testando os exemplos com Docker

O container `otaviog/reconstrucao3d-open3d-python` está disponível no Dockerhub.
Comando de pull:

```shell
docker pull otaviog/reconstrucao3d-open3d-python:latest
```

Execução para mapear container ao sistema de janelas do Ubuntu:

```shell
docker run --gpus all\
   	--user=`id -u`:`id -g`\
   	--env="DISPLAY"\
   	--env=NVIDIA_DRIVER_CAPABILITIES=all\
   	--env=XAUTHORITY\
   	--volume="/etc/group:/etc/group:ro"\
   	--volume="/etc/passwd:/etc/passwd:ro"\
   	--volume="/etc/shadow:/etc/shadow:ro"\
   	--volume="/etc/sudoers.d:/etc/sudoers.d:ro"\
   	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"\
   	--workdir=/reconstrucao3d-open3d-python\
   	-it otaviog/reconstrucao3d-open3d-python /bin/bash
```

Ou use o Makefile caso o repositório estiver clonado:

```shell
make docker-run
```
# Referências

Este repositório contém dados dos seguintes trabalhos:

* [INDOOR LIDAR-RGBD SCAN DATASET](http://redwood-data.org/indoor_lidar_rgbd/index.html) Park, Jaesik, Qian-Yi Zhou, and Vladlen Koltun. "Colored point cloud registration revisited." In Proceedings of the IEEE International Conference on Computer Vision, pp. 143-152. 2017. [Domínio público](http://redwood-data.org/indoor_lidar_rgbd/license.html)
* [T-LESS](http://cmp.felk.cvut.cz/t-less) Hodan, Tomáš, Pavel Haluza, Štepán Obdržálek, Jiri Matas, Manolis Lourakis, and Xenophon Zabulis. "T-LESS: An RGB-D dataset for 6D pose estimation of texture-less objects." In 2017 IEEE Winter Conference on Applications of Computer Vision (WACV), pp. 880-888. IEEE, 2017. [Creative Commons Attribution-ShareAlike 4.0](http://cmp.felk.cvut.cz/t-less/)
