FROM nvidia/opengl:1.2-glvnd-devel-ubuntu18.04
MAINTAINER otaviog
LABEL maintainer="Otavio Gomes <otavio.gomes@eldorado.org.br>"

RUN apt update && apt -yq install python3 python3-venv build-essential python3-dev

RUN mkdir /reconstrucao3d-open3d-python
WORKDIR /reconstrucao3d-open3d-python

ENV VIRTUAL_ENV=/reconstrucao3d-open3d-python/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip3 install -U pip

COPY requirements.txt .
RUN pip3 install -r requirements.txt

ADD . /reconstrucao3d-open3d-python
