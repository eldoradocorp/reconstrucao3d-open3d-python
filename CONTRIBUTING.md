# CONTRIBUTING

# Virtualenv

```shell
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
pip3 install -r requirements-dev.txt
```

# Manutenção

Use o `Makefile` para buildar o docker e análise estática.
