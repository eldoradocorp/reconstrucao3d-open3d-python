about:
	@echo "Manutenção do projeto"

##
# Docker
docker-build:
	docker build -t otaviog/reconstrucao3d-open3d-python .

docker-run:
	docker run --gpus all\
		--user=`id -u`:`id -g`\
		--env="DISPLAY"\
		--env=NVIDIA_DRIVER_CAPABILITIES=all\
		--env=XAUTHORITY\
		--volume="/etc/group:/etc/group:ro"\
		--volume="/etc/passwd:/etc/passwd:ro"\
		--volume="/etc/shadow:/etc/shadow:ro"\
		--volume="/etc/sudoers.d:/etc/sudoers.d:ro"\
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"\
		--workdir=/reconstrucao3d-open3d-python\
		-it otaviog/reconstrucao3d-open3d-python /bin/bash

TAG=`git describe --tags --abbrev=0 | cut -c 2-`
docker-push:
	docker tag otaviog/reconstrucao3d-open3d-python otaviog/reconstrucao3d-open3d-python:${TAG}
	docker push otaviog/reconstrucao3d-open3d-python:${TAG}
	docker push otaviog/reconstrucao3d-open3d-python

docker-dive: # https://github.com/wagoodman/dive
	dive otaviog/reconstrucao3d-open3d-python

##
# Qualidade
pylint:
	python -m pylint *.py

pep8:
	python -m autopep8 --recursive --in-place .
