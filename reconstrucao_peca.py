#!/usr/bin/env python
# coding: utf-8
"""
Demonstra como usar a odometria visual para reconstruir uma peça
usando um volume TSDF.

Escreve no arquivo peca.ply.
"""

from pathlib import Path

import open3d
import numpy as np
from tqdm import tqdm

# Imagens do conjunto de dados: T-Less (http://cmp.felk.cvut.cz/t-less/)
seq_path = Path(__file__).parent.absolute() / "t-less/08"
image_pair_list = list(zip(
    [str(p) for p in sorted((seq_path / "rgb").glob("*.png"))],
    [str(p) for p in sorted((seq_path / "depth").glob("*.png"))]))

# Parâmetros ou fatores intrínsecos
camera_intrinsic = open3d.camera.PinholeCameraIntrinsic(
    400, 400,
    1076.74064739, 1075.17825536, # Distâncias focais x e y [1]
    219.98264967, 154.59181836) # Centro de projeção
# [1] Existe uma distancia focal por eixo (x e y) pois esses
# valores não codificam apenas a distância focal, eles também
# possui uma escala do aspect ratio horizontal e vertical
# dos sensores dos pixels da câmera.

# O T-less usa configurações exóticas de profundidade
DEPTH_TRUNC = 75.0 # Especifica a distancia máxima da profundidade
DEPTH_SCALE = 100.0 # Escala da profundidade

prev_rgbd_image = None
T = np.eye(4)
T_list = []
for color_image, depth_image in tqdm(image_pair_list):
    color_image = open3d.io.read_image(color_image)

    depth_image = open3d.io.read_image(depth_image)
    bg_mask = np.asarray(color_image).sum(2) < 10
    np.asarray(depth_image)[bg_mask] = 0

    rgbd_image = open3d.geometry.RGBDImage.create_from_color_and_depth(
        color_image, depth_image, depth_scale=DEPTH_SCALE,
        depth_trunc=DEPTH_TRUNC)

    if prev_rgbd_image is None:
        prev_rgbd_image = rgbd_image
        T_list.append(T)
        continue

    odo_success, M, _ = open3d.odometry.compute_rgbd_odometry(
        rgbd_image, prev_rgbd_image, camera_intrinsic,
        option=open3d.odometry.OdometryOption(max_depth_diff=0.3, max_depth=DEPTH_TRUNC))
    T = T @ M   # Atenção pelo operador @ e não *
    T_list.append(T.copy())

    prev_rgbd_image = rgbd_image

tsdf = open3d.integration.ScalableTSDFVolume(
    0.05, 1, open3d.integration.TSDFVolumeColorType.RGB8)
for i, (color_image, depth_image) in enumerate(image_pair_list):
    color_image = open3d.io.read_image(color_image)
    depth_image = open3d.io.read_image(depth_image)

    T = T_list[i]

    rgbd_image = open3d.geometry.RGBDImage.create_from_color_and_depth(
        color_image, depth_image, convert_rgb_to_intensity=False, depth_scale=DEPTH_SCALE,
        depth_trunc=DEPTH_TRUNC)

    tsdf.integrate(rgbd_image, camera_intrinsic, np.linalg.inv(T))

mesh = tsdf.extract_triangle_mesh()

open3d.visualization.draw_geometries([tsdf.extract_triangle_mesh()])

open3d.io.write_triangle_mesh("peca.ply", mesh)
