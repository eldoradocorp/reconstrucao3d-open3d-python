#!/usr/bin/env python
# coding: utf-8
"""
Exemplo de odometria visual quadro-a-quadro.
Estima a trajetória e exibe as nuvems de pontos alinhadas.
"""

from pathlib import Path

import open3d
import numpy as np
from tqdm import tqdm


seq_path = Path(__file__).parent.absolute() / "sequencia-rgbd"
image_pair_list = list(zip([str(p) for p in sorted(seq_path.glob("*.jpg"))],
                           [str(p) for p in sorted(seq_path.glob("*.png"))]))

camera_intrinsic = open3d.camera.PinholeCameraIntrinsic(640, 480,
                                                        525, 525, 319.5, 239.5)

prev_rgbd_image = None
T = np.eye(4)
T_list = []
for color_image, depth_image in tqdm(image_pair_list):
    color_image = open3d.io.read_image(color_image)
    depth_image = open3d.io.read_image(depth_image)
    rgbd_image = open3d.geometry.RGBDImage.create_from_color_and_depth(
        color_image, depth_image, convert_rgb_to_intensity=True)

    if prev_rgbd_image is None:
        prev_rgbd_image = rgbd_image
        T_list.append(T)
        continue

    odo_success, M, _ = open3d.odometry.compute_rgbd_odometry(
        rgbd_image, prev_rgbd_image, camera_intrinsic)
    T = T @ M  # Atenção pelo operador @ e não *
    T_list.append(T)

    prev_rgbd_image = rgbd_image

###
# Exibe as nuvens de pontos:
pcds = []
for (color_image, depth_image), T in list(zip(image_pair_list, T_list))[::50]:
    color_image = open3d.io.read_image(color_image)
    depth_image = open3d.io.read_image(depth_image)
    rgbd_image = open3d.geometry.RGBDImage.create_from_color_and_depth(
        color_image, depth_image, convert_rgb_to_intensity=False)

    pcd = open3d.geometry.PointCloud.create_from_rgbd_image(
        rgbd_image, camera_intrinsic)
    pcd = pcd.transform(T)
    pcds.append(pcd)

open3d.visualization.draw_geometries(pcds)
