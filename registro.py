#!/usr/bin/env python
"""
Como registrar um par de imagens RGBD.
"""

import open3d

rgbd_image1 = open3d.geometry.RGBDImage.create_from_color_and_depth(
    open3d.io.read_image("imagem_rgb01.jpg"),
    open3d.io.read_image("imagem_depth01.png"),
    convert_rgb_to_intensity=False)
rgbd_image2 = open3d.geometry.RGBDImage.create_from_color_and_depth(
    open3d.io.read_image("imagem_rgb02.jpg"),
    open3d.io.read_image("imagem_depth02.png"),
    convert_rgb_to_intensity=False)
camera_intrinsic = open3d.camera.PinholeCameraIntrinsic(
    open3d.camera.PinholeCameraIntrinsicParameters.PrimeSenseDefault)

odo_success, M, information = open3d.odometry.compute_rgbd_odometry(
    rgbd_image2, rgbd_image1, camera_intrinsic)
print("Matriz de alinhamento: \n", M)

pcd1 = open3d.geometry.PointCloud.create_from_rgbd_image(
    rgbd_image1, camera_intrinsic)
pcd2 = open3d.geometry.PointCloud.create_from_rgbd_image(
    rgbd_image2, camera_intrinsic)

open3d.visualization.draw_geometries([pcd1, pcd2])

pcd2_reg = pcd2.transform(M)
open3d.visualization.draw_geometries([pcd1, pcd2_reg])
