#!/usr/bin/env python
"""
Exemplo: como ler uma imagem colorida e de profundidade para criar
uma imagem RGBD, extrair sua nuvem de pontos e exibir.
"""
import open3d

color_image = open3d.io.read_image("imagem_rgb.jpg")
depth_image = open3d.io.read_image("imagem_depth.png")

rgbd_image = open3d.geometry.RGBDImage.create_from_color_and_depth(
    color_image, depth_image,
    convert_rgb_to_intensity=False)

camera_intrinsic = open3d.camera.PinholeCameraIntrinsic(
    open3d.camera.PinholeCameraIntrinsicParameters.Kinect2DepthCameraDefault)
pcd = open3d.geometry.PointCloud.create_from_rgbd_image(
    rgbd_image, camera_intrinsic)

open3d.visualization.draw_geometries([pcd])
