#!/usr/bin/env python
"""
Demonstra como usar a odometria visual para reconstruir uma cena
usando um volume TSDF.

Escreve no arquivo cena.ply.
"""
from pathlib import Path

import open3d
import numpy as np
from tqdm import tqdm

seq_path = Path(__file__).parent.absolute() / "sequencia-rgbd"
image_pair_list = list(zip([str(p) for p in sorted(seq_path.glob("*.jpg"))],
                           [str(p) for p in sorted(seq_path.glob("*.png"))]))

camera_intrinsic = open3d.camera.PinholeCameraIntrinsic(640, 480,
                                                        525, 525, 319.5, 239.5)
#camera_intrinsic = open3d.camera.PinholeCameraIntrinsic(
#    open3d.camera.PinholeCameraIntrinsicParameters.Kinect2DepthCameraDefault)

prev_rgbd_image = None
T = np.eye(4)
T_list = []
for color_image, depth_image in tqdm(image_pair_list):
    color_image = open3d.io.read_image(color_image)
    depth_image = open3d.io.read_image(depth_image)
    rgbd_image = open3d.geometry.RGBDImage.create_from_color_and_depth(
        color_image, depth_image)

    if prev_rgbd_image is None:
        prev_rgbd_image = rgbd_image
        T_list.append(T)
        continue

    odo_success, M, _ = open3d.odometry.compute_rgbd_odometry(
        rgbd_image, prev_rgbd_image, camera_intrinsic)
    T = T @ M  # Atenção pelo operador @ e não *
    T_list.append(T.copy())

    prev_rgbd_image = rgbd_image

tsdf = open3d.integration.ScalableTSDFVolume(
    10.0 / 512, 0.05, open3d.integration.TSDFVolumeColorType.RGB8)
for i, (color_image, depth_image) in enumerate(image_pair_list):
    color_image = open3d.io.read_image(color_image)
    depth_image = open3d.io.read_image(depth_image)

    T = T_list[i]

    rgbd_image = open3d.geometry.RGBDImage.create_from_color_and_depth(
        color_image, depth_image, convert_rgb_to_intensity=False)

    tsdf.integrate(rgbd_image, camera_intrinsic, np.linalg.inv(T))

mesh = tsdf.extract_triangle_mesh()

open3d.visualization.draw_geometries([tsdf.extract_triangle_mesh()])

open3d.io.write_triangle_mesh("cena.ply", mesh)
